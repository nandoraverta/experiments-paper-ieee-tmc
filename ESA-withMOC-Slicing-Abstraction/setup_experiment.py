from datetime import date
from brufn.experiment_generator import BRUF_1, BRUF_2, BRUF_3, BRUF_4, CGR_MODEL350, CGR_FA, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_4, CGR_BRUF_POWERED

DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
INPUT_CP = 'scenario/walker-12leo-10gs-100gt-24hrs-contact-plan.txt'
ROUTING_ALGORITHMS = ['BRUF_1']#[ BRUF_4, SPRAY_AND_WAIT_4] #[CGR_BRUF_POWERED]#[BRUF_1, BRUF_2, BRUF_3, CGR_FA, CGR_MODEL350, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3] #[BRUF_1, BRUF_2, BRUF_3, BRUF_4, SPRAY_AND_WAIT_4]

COLD_SPOT_EIDS = [11] #[11, 50, 110]
HOT_SPOTS_EIDS = [1, 9]
SATELLITE_EIDS = list(range(111, 123))
NUM_OF_REPS = 300
TS_DURATION_IN_SECONDS = 60
HOURS_RANGE = [3600 * h for h in range(2, 26, 2)]
OUTPUT_PATH = 'exp-28-10-2019'#f'exp-{date.today().strftime("%d-%m-%Y")}'

ALGORITHM_LABELS = {
CGR_FA: 'CGR-FaultsAware',
BRUF_1: 'BRUF-1',
BRUF_2: 'BRUF-2',
BRUF_3: 'BRUF-3',
BRUF_4: 'BRUF-4',
SPRAY_AND_WAIT_2: 'SprayAndWait-2',
SPRAY_AND_WAIT_3: 'SprayAndWait-3',
SPRAY_AND_WAIT_4: 'SprayAndWait-4',
CGR_MODEL350: 'CGR-DelTime',
CGR_BRUF_POWERED: 'CGR-BRUF'
}

ALGORITHM_COLORS = {
CGR_FA: 'black',
BRUF_1: 'red',
BRUF_2: 'green',
BRUF_3: 'blue',
BRUF_4: 'orange',
SPRAY_AND_WAIT_2: 'darkcyan',
SPRAY_AND_WAIT_3: 'slateblue',
SPRAY_AND_WAIT_4: 'pink',
CGR_MODEL350: 'purple',
CGR_BRUF_POWERED:'darkmagenta'
}
