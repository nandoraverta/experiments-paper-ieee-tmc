import os
from brufn.utils import getListFromFile, average, print_str_to_file, prom_llist
from setup_experiment import ROUTING_ALGORITHMS, NET_RANGE

EXP_PATH = 'exp-07-10-2019'
METRICS = [("appBundleReceived:count","Delivered Bundles"),("deliveryRatio","Delivery Ratio"), ("dtnBundleSentToCom:count","Transmitted bundles"), ("appBundleReceivedDelay:mean","Mean Delay per Bundle"), ("appBundleReceivedHops:mean","Mean Hops per Bundle"), ("sdrBundleStored:timeavg", "Mean Bundles in SDR"), ('EnergyEfficiency', 'Energy Efficiency')]

METRIC_AV_OUTPUT_DIR = os.path.join(EXP_PATH, 'metrics_av'); os.makedirs(METRIC_AV_OUTPUT_DIR, exist_ok=True)
for metric, metric_label in METRICS:
    functions_by_alg = dict((algorithm, []) for algorithm in ROUTING_ALGORITHMS)
    for net in NET_RANGE:
        net_path = os.path.join(EXP_PATH, f'net-{net}')
        for algorithm in ROUTING_ALGORITHMS:
            alg_path = os.path.join(net_path, algorithm)
            metric_path = os.path.join(alg_path, 'metrics', f'METRIC={metric}.txt')
            functions_by_alg[algorithm].append(getListFromFile(metric_path))
    
    for algorithm in ROUTING_ALGORITHMS:
        os.makedirs(os.path.join(METRIC_AV_OUTPUT_DIR, algorithm), exist_ok=True)
        print_str_to_file(str(prom_llist(functions_by_alg[algorithm])), os.path.join(METRIC_AV_OUTPUT_DIR, algorithm, f'METRIC={metric}.txt'))

