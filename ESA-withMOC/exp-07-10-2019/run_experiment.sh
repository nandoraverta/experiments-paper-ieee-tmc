#!/usr/bin/env bash
base_dir="$PWD" &&

cd Target-11 &&
bash run_experiment.sh &&
cd ${base_dir}&&

cd Target-50 &&
bash run_experiment.sh &&
cd ${base_dir}&&

cd Target-110 &&
bash run_experiment.sh &&
cd ${base_dir}