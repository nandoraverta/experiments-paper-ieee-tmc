from brufn.experiment_generator import BRUF_1, BRUF_2, BRUF_3, BRUF_4, CGR_MODEL350, CGR_FA, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_4, CGR_2COPIES, CGR_HOPS, CGR_BRUF_POWERED

ALGORITHM_LABELS = {
CGR_FA: 'CGR-FaultsAware',
BRUF_1: 'BRUF-1',
BRUF_2: 'BRUF-2',
BRUF_3: 'BRUF-3',
BRUF_4: 'BRUF-4',
SPRAY_AND_WAIT_2: 'SprayAndWait-2',
SPRAY_AND_WAIT_3: 'SprayAndWait-3',
SPRAY_AND_WAIT_4: 'SprayAndWait-4',
CGR_MODEL350: 'CGR-DelTime',
CGR_2COPIES: 'CGR-2',
CGR_HOPS: 'CGR-Hops',
CGR_BRUF_POWERED: 'CGR-BRUF'
}

ALGORITHM_COLORS = {
CGR_FA: 'black',
BRUF_1: 'red',
BRUF_2: 'green',
BRUF_3: 'blue',
BRUF_4: 'orange',
SPRAY_AND_WAIT_2: 'darkcyan',
SPRAY_AND_WAIT_3: 'slateblue',
SPRAY_AND_WAIT_4: 'pink',
CGR_MODEL350: 'purple',
CGR_2COPIES: 'yellow',
CGR_HOPS: 'lightblue',
CGR_BRUF_POWERED:'darkmagenta'
}

ALGORITHM_LINE_STYLES = {
CGR_FA: '--^',
BRUF_1: '--o',
BRUF_2: '--s',
BRUF_3: '--v',
BRUF_4: '--p',
SPRAY_AND_WAIT_2: '--x',
SPRAY_AND_WAIT_3: '--+',
SPRAY_AND_WAIT_4: '--<',
CGR_MODEL350: '--H',
CGR_2COPIES: '--h',
CGR_HOPS: '-->',
CGR_BRUF_POWERED: '--|'
}

