#!/usr/bin/env bash

virtualenv venv -p /usr/bin/python3.6 &&
source venv/bin/activate &&
pip install -r requirements.txt &&
working_dir=$PWD &&

cd libs/ &&
tar xvzf brufn-0.0.2.tar.gz
cd brufn-0.0.2/ &&
python setup.py install &&
cd $working_dir &&

cd libs/ &&
tar xvzf svg_stack-0.0.1.tar.gz &&
cd svg_stack-0.0.1 &&
python setup.py install &&
cd $working_dir
