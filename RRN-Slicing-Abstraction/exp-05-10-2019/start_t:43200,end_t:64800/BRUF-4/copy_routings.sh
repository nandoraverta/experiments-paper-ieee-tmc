#!/bin/bash 

PF_RANGE=( 0.00 0.10 0.20 0.30 0.40 0.50 0.60 0.70 0.80 0.90 1.00)
OUTPUT_DIR="routing_files2"

mkdir "${OUTPUT_DIR}"
for pf in "${PF_RANGE[@]}"; do
    mkdir "${OUTPUT_DIR}/pf=${pf}"
    for source in $(seq 0 21); do
       #echo "cp routing_files/pf=${pf}/mc-dtnsim-from:${source}-to:38-${pf}.json routing_files2/pf=${pf}" 
       cp "routing_files/pf=0.00/mc-dtnsim-from:${source}-to:38-${pf}.json" "routing_files2/pf=${pf}/todtnsim-${source}-38-${pf}.json"
       #echo "routing_files/pf=${pf}/mc-dtnsim-from:${source}-to:38-${pf}.json routing_files/pf=${pf}/todtnsim-from:${source}-to:38-${pf}.json"         
       #mv  "routing_files/pf=${pf}/todtnsim-$((source - 1))-38-${pf}.json" "routing_files/pf=${pf}/todtnsim-${source}-38-${pf}.json"
    done
done
