#!/bin/bash 

if [ $# -eq 3 ]
then
    EXP_PATH=$1
    CP=$2
    ALGORITHM=$3

    echo ${EXP_PATH}/${CP}/${ALGORITHM}/results
    rm -r ${EXP_PATH}/${CP}/${ALGORITHM}/results

else
    echo $#
    echo "[Error] delete_results: You must put delete_results"
fi
