from datetime import date
from brufn.experiment_generator import BRUF_1, BRUF_2, BRUF_3, BRUF_4, CGR_MODEL350, CGR_FA, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_4, CGR_2COPIES, CGR_HOPS, CGR_BRUF_POWERED

DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
GROUND_TARGETS_EIDS = [x for x in range(7, 32) if x not in [11,17,28]]
SATELLITES = list(range(32, 48))
GS_EID = 1


ALGORITHM_LABELS = {
CGR_FA: 'CGR-FaultsAware',
BRUF_1: 'BRUF-1',
BRUF_2: 'BRUF-2',
BRUF_3: 'BRUF-3',
BRUF_4: 'BRUF-4',
SPRAY_AND_WAIT_2: 'SprayAndWait-2',
SPRAY_AND_WAIT_3: 'SprayAndWait-3',
SPRAY_AND_WAIT_4: 'SprayAndWait-4',
CGR_MODEL350: 'CGR-DelTime',
CGR_2COPIES: 'CGR-2',
CGR_HOPS: 'CGR-Hops',
CGR_BRUF_POWERED: 'CGR-BRUF'
}
ROUTING_ALGORITHMS = [BRUF_4]
ALGORITHM_COLORS = {
CGR_FA: 'black',
BRUF_1: 'red',
BRUF_2: 'green',
BRUF_3: 'blue',
BRUF_4: 'orange',
SPRAY_AND_WAIT_2: 'darkcyan',
SPRAY_AND_WAIT_3: 'slateblue',
SPRAY_AND_WAIT_4: 'pink',
CGR_MODEL350: 'purple',
CGR_2COPIES: 'yellow',
CGR_HOPS: 'lightblue',
CGR_BRUF_POWERED:'darkmagenta'
}
